<?php
include 'db.php';
include 'functions.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
            <table class="table table-condensed">
                <caption><h3>Водители</h3></caption>
                <thead>
                <tr>
                    <th>Имя водителя</th>
                    <th>Марка автомобиля</th>
                    <th>Дата регистрации</th>
                    <th>Баланс, руб</th>
                </tr>
                <tbody>
                <form class="form-group" method="post">
                    <?php
                    removeDriver($link);
                    listDrivers($link) ;
                    ?>

                </form>
                </tbody>
                </thead>

            </table>

            <a href="addDriver.php">Добавить водителя</a>

    </div>
</div>
</body>
</html>