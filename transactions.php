<?php
include 'db.php';
include 'functions.php';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div class="row">
        <table class="table table-condensed">
            <caption><h3>Истории транзакции водителя</h3></caption>
            <thead>
            <tr>
                <th>Тип транзакции</th>
                <th>Премии</th>
                <th>Дата</th>
            </tr>
            <form method="post">
                <?php transactionsDriver($link) ?>

            </form>

        </table>
        <input type="button" onclick="history.back();" value="Назад"/>
    </div>
</div>
</body>
</html>